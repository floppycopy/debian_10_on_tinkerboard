## NOTE:
This only applies to the Tinkerboard. The Tinkerboard S is different due to on-board storage. If you mess around with that be sure that you can somehow write something to the eMMC without needing to start it; that probably is not a lot of fun...  
Also note that I didn't bother to **e.g.** get wifi/bluetooth and audio over HDMI to work since I don't need that. (wifi/bt will probably need some firmware)  

### Introduction
In order to have up to date software without needing to compile kernels all the time I wanted to run debian with its default kernel on my Tinkerboard. Since I haven't found any notes on how to do this all the while some things were not really straight forward for me I wrote this text in order to help others who want to do the same or a similar thing. (Probably not too common but some parts might be helpful for other boards too.) I'm not sure whether this works well on anything but linux. (It probably can also be done on *BSD systems, no idea about others.)

### The usual warning
**I can't guarantee that this won't lead to any damages or that it works at all.** It worked fine for me; **however**, incorrectly connecting stuff can lead to big problems. (If you mix up e.g. UART/VCC pins you can break your tinkerboard, your USB-UART-controller, maybe your Computer; if some power supply is faulty (e.g. not properly separated from the primary side) it can be *much* worse.)  
Be sure that you know what you are doing when dealing with UART and especially power supplies, don't forget that the commonly ungrounded 5V USB power supplies can float around anywhere (speaking of voltage differences between the output of the power supply and ground). This usually isn't a problem but can probably easily damage UART ports.  
The same goes for software: It may be possible to turn your Tinkerboard into a relatively lightweight fancy-looking brick that doesn't do much. (It shouldn't be too easy since everything is stored on the SD-Card but I'm sure it's possible in one way or another.)

### Needed experience
This is a rough list of what you'll need to know in order to follow these steps. It's probably a good idea to read through this wohle text and check whether I have overlooked anything important.  
I don't think any of this is really difficult; however, it certainly can be when you are doing this for the first time. That's what you should to know about:  
* (the command line of course)
* compiling source code from projects using make  
* connecting stuff over UART  
* knowing about UART voltage levels  
* writing stuff to the correct device files using dd  

### Needed hardware
Having something to connect to the tinkerboard's UART interface is essential for this approach. Luckily USB-UART-controllers are pretty cheap nowadays (I bought one *locally* for ~3€); also, we don't need any fancy features, we only need the RX, TX and GND pins. The UART uses 3.3V.  
You'll also need an empty Micro-SD-Card for the tinkerboard, a power supply to supply your tinkerboard, an appropriate monitor to get video over HDMI from your tinkerboard (somewhat optional) and, of course, a tinkerboard.

### Links where I got most of the stuff I didn't figure out myself from:
compiling u-boot: https://tinkerboarding.co.uk/forum/thread-1829-post-7555.html#pid7555  
getting UART output: https://github.com/rockchip-linux/u-boot/issues/4

### (using `screen` )
`screen` is a terminal multiplexer that is installed by default on many distros; it also can be used to conveniently talk to a device over UART. (That's not very UNIX of it...)  
* Starting screen (assuming your UART is at /dev/ttyUSB0; 115200 baud is the default for the tinkerboard UART in u-boot it seems): `screen /dev/ttyUSB0 115200`  
* exiting screen: press `Ctrl+a \` - you'll be prompted whether you want to exit.  
* switching windows: `Ctrl+a <num>`, e.g. `Ctrl+a 1` to switch to window 1  
* switching windows in screen inside screen: `Ctrl+a a 1` to switch to window 1 (this is relevant for the debian installer)  

### connecting to the serial console
The pre-compiled u-boot from ASUS already writes something to UART1. It probably is a good idea to test this with the default image to make any search for errors easier.  
The UART can be connected just like on a Raspberry Pi: The same pins are used. (On the header there's +5V +5V GND TX RX ...; connect your USB-to-UART-GND to the tinkerboard GND, the USB-to-UART-RX to the TX on the Tinkerboard and do the equivalent for TX/RX. Adding Resistors in series is probably a good idea in case you incorrectly connect something; you can probably use something between 220Ω and 560Ω (this does not guarantee that nothing breaks when connecting e.g. TX to GND but makes it a bit less likely). You can roughly test whether you chose too large resistors by connecting your USB-to-UART RX to TX (over the two resistors) and observe whether you get an echo when writing to the tty.)

### getting u-boot

`git clone https://gitlab.denx.de/u-boot/u-boot.git`  
`cd u-boot`  
The last version that worked for me was v2019.01, afterwards the SPL got too big for the internal SRAM so I used this version:  
`git checkout v2019.01`  
(see also http://u-boot.10912.n7.nabble.com/Bring-tinker-rk3288-back-into-SPL-size-limit-td372197.html, there is also a suggested workaround)

### preparations for compiling u-boot
Unless you are on ARM you'll need a cross compiler; in Ubuntu the package "gcc-arm-linux-gnueabihf" is suitable. (The tools are called arm-linux-gnueabihf-{gcc,ld,ar,...}.)  
`export CROSS_COMPILE=arm-linux-gnueabihf-`  
Let's replace UART2 with UART1 - UART2 is said to be some default for rockchip devices, UART1 however is more convenient to access.  
`sed -i 's/uart2/uart1/g' arch/arm/dts/rk3288-tinker.dts`  
Since I don't use network boot (maybe a good idea when not using the most up-to-date u-boot) I removed it from the appropriate file; also, I changed the boot order to first try booting from a USB device. Note that this will make booting slower (but it makes booting from a USB drive simpler).  
`sed -i '/\(DHCP\|PXE\)/d' include/configs/tinker_rk3288.h`  
`sed -i 's/func(USB, usb, 0) \\/func(MMC, mmc, 1)/g' include/configs/tinker_rk3288.h`  
`sed -i 's/func(MMC, mmc, 1) \\/func(USB, usb, 0) \\/g' include/configs/tinker_rk3288.h`  

(You could of course also just edit the file: Remove the lines containing DHCP and PXE and swap the USB and MMC lines; be careful, the first of the latter two lines needs the '\\'
at the end, the other one doesn't.)  

Get the default config:  
`make tinker-rk3288_defconfig`  

Disable networking if not needed:  
("Networking support" can be found at the top level; press "n", save the config and quit menuconfig.)  
`make menuconfig`

### build and install u-boot

Build u-boot:  
As parameter for make use "-j<num of threads your CPU has>", e.g. "-j8" if it has 8 threads  
(this produces lots of output, including exactly two warnings (related to ethernet it seems) on my setup)  
`make -j8`  

create the u-boot image (it says "Data Size:    28672 bytes" using my setup; this is what gets too large with newer versions of u-boot currently)  
`mkimage -n rk3288 -T rksd -d spl/u-boot-spl-dtb.bin u-boot.img`  

Append u-boot.bin to u-boot.img (I think this simply appends u-boot to the SPL; I got this from the linked tinkerboard forum entry)  
`cat u-boot.bin >> u-boot.img`

Format the SD-Card (MBR / "MS-DOS" partition table). It should have a little FAT32 partition at the beginning (~64MB on the manufacturer image, 128MB is enough for sure).  
Leave a few MB empty in front of the FAT partition (roughly 4MB were left empty on the manufacturer image) - this is where u-boot will be stored later on (similarly to how bootloaders are stored on MBR formatted disks in "normal" computers). (The default 1MB left empty by gparted should be large enough but a few MB more (I chose 4) aren't a lot nowadays. This may also be convenient in case you later want to mess around with different u-boot settings that result in larger binaries.)  
(The Label of the FAT-Partition shouldn't matter but I called it "boot".)  

Write u-boot to the SD-card (as usual, be *really* careful about which device you write this to)  
`sudo dd if=./u-boot.img of=/dev/sdX seek=64 oflag=sync`

### Time to check whether we get anything over UART!
This is the output I get:  
```
MMC:   dwmmc@ff0c0000: 1
Loading Environment from MMC... *** Warning - bad CRC, using default environment

In:    serial
Out:   serial
Err:   serial
Model: Tinker-RK3288
Hit any key to stop autoboot:  2
```

(you can now unplug your tinkerboard, it seems to be working)  
  
Once the timeout is reached u-boot will start looking for devices. Time to set up more stuff.  

### preparations for installing debian (including starting grub)
Fetch the current netinst iso from debian (https://cdimage.debian.org/debian-cd/current/armhf/iso-cd/), in my case that was debian-10.6.0-armhf-netinst.iso (as of the time of writing 10.7.0 is already available but there shouldn't be much difference apart from newer software I think) and write it to a USB-drive (e.g. using dd). This may take some time depending on your USB-drive; you can continue with the devicetree in the mean time.  
Mount the first partition from the iso somehow (the distro I use has a convenient "disk image mounter" to do that) and copy the devicetree from install.ahf/device-tree/rk3288-tinker.dtb somewhere where you want to modify it (e.g. some temporary directory).  

decompile the devicetree:  
`dtc -I dtb -O dts rk3288-tinker.dtb > tmp.dts`  

Set the regulator for the SD-Card to "always on" (otherwise reboot won't work)  
(see discussion [here](https://forum.armbian.com/topic/4582-asus-tinker-board-reboot/), commit 579f52f680b56 in the linux kernel and the tinkerboard schematic from ASUS)  
`sed -i 's/LDO_REG5 {/LDO_REG5 {\n\t\t\t\t\tregulator-always-on;/g' tmp.dts`  

recompile the devicetree  
`dtc -I dts -O dtb tmp.dts > rk3288-tinker.dtb`  

Copy the devicetree to the FAT partition of the SD-Card and plug it and the USB-drive (once the image writing is done) into the Tinkerboard, then start the Tinkerboard.  
If everything works you should now get a GRUB menu over the UART (including colours!). However, interestingly we can't just select "install". We need to edit the entry and add the "devicetree" option. (I have no idea why the kernel doesn't seem to get the dt over EFI...)  

Edit the entry "Install..." in GRUB over UART: Insert "devicetree (hd1,msdos1)/rk3288-tinker.dtb" (to find out whether "(hd1,msdos1)" is correct you can simply play around
with "ls" in the GRUB shell (Ctrl+C to get the shell, ESC (maybe twice) to leave it again) or play around with tab completion (this works in the editing menu - how convenient!)).  

The kernel also needs additional parameters: We want to get status messages over UART. Thus, add "earlyprintk console=ttyS1,115200n8" at the end of the "linux"-entry.  
(e.g. "linux    /install.ahf/vmlinuz earlyprintk console=ttyS1,115200n8")  
Be careful with long copy-pastes: With long text characters may get lost. (I guess that this has to do with the interrupt handling speed in GRUB on the Tinkerboard;
hardware flow control could probably prevent this but that would need more pins...)  

### Time to boot and install debian!
At first we get  
```
EFI stub: Booting Linux Kernel...
EFI stub: Using DTB from configuration table
EFI stub: Exiting boot services and installing virtual address map...
```
...which we also get without specifying a devicetree; however, after that (and some time) we get messages from the kernel booting and then from the debian installer.  

Do the normal debian install; don't worry about the framebuffer console, you should simply get one once you boot from the sd-card.
(Be careful not to overwrite the FAT partition of course. E.g. I created a 8GB ext4 partition right after the FAT partition for the rootfs in the installer.)  
Set the FAT partition as "EFI system partition" (select it in the partitioning menu and set "use as" appropriately)  
(You may want to set up a swap partition, I didn't do that; it for sure isn't needed for the installation and in case I later need one I'd rather put it somewhere else than the SD card anyway.)  

### installing the bootloader

At some point installing grub fails (considering it says "grub-install dummy" it probably is supposed to fail - bootloaders are often weird on ARM devices).
Enter away the error messages and switch to one of the shells.
Check whether the FAT partition is mounted to /target/boot/efi - it should be mounted there, otherwise things won't work I think.  
`mount`

If everything worked (I hope so, otherwise you'll need to manually mount the partition and add it to fstab later on - I think that should be everything that needs to be done in that
case) install grub:  

Bind-mount some useful stuff: (/proc and /run are already mounted by default)  
`mount -o bind /sys /target/sys`  
`mount -o bind /dev /target/dev`  
`mount -o bind /dev/pts /target/dev/pts`  

chroot to the newly installed system  
`chroot /target`  

get a nice shell and prompt  
`bash`  

install grub  
("--removable" is required for the default distroboot cmd in u-boot as far as I know)  
`grub-install --efi-directory=/boot/efi/ --removable --target=arm-efi`  

(output: (well, hopefully)  
```
Installing for arm-efi platform.
Installation finished. No error reported.
```
)

NOTE: If you do not have a monitor over HDMI connected edit /etc/default/grub now so that GRUB\_CMDLINE\_LINUX is set to "earlyprintk console=ttyS1,115200n8" (without the "quiet"-parameter, at least now we'd want to see errors). (Of course it is also possible to edit the entry in GRUB at boot but that has to be done at every start.)

generate grub config:  
`update-grub`  

(
```
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-4.19.0-13-armmp-lpae
Found initrd image: /boot/initrd.img-4.19.0-13-armmp-lpae
Found linux image: /boot/vmlinuz-4.19.0-11-armmp-lpae
Found initrd image: /boot/initrd.img-4.19.0-11-armmp-lpae
done
```
)

Exit from bash and chroot.  

unmount bind mounts:  
`umount /target/sys`  
`umount /target/dev/pts`  
`umount /target/dev`  

Switch back to the installer.  
Select "continue without boot loader" (we already installed it but the installer doesn't know that of course).  

After being done be quick removing your USB drive (but don't be too hasty, don't break anything (-; – in the worst case you'll get to the installer again.) to boot from the SD card.  

(Interestingly after reboots entering stuff into `screen` occasionally does not seem to work properly in my case; when that happens I just quit screen and start it again. (getting grub to redraw can be done by pressing "e" and then Esc (the latter one twice it seems).))

If you haven't set any kernel parameters you won't get any output on UART; however, you should get output on the framebuffer (e.g. on a screen connected to the HDMI port) after some time (maybe 30 seconds or so).  
If you need the console make sure to pass the kernel command line parameters from before in GRUB again and best set that up in the grub config (see somewhere above).  

Done! From now on everything should work rather normally.

--------------------------------------------------------

I messed around a bit with some more stuff that is beyond the scope of this writeup and I didn't really document it well; I'll dump it here anyway, maybe someone finds it useful.

(to get the proper keyboard layout for my keyboard I needed to install "keyboard-configuration" and "console-setup"; for configuration use ´dpkg-reconfigure keyboard-configuration´ )  

´sudo apt-get install lxde-core firefox-esr alsa-utils command-not-found´  

´reboot´ (or find out what service to start or restart for alsa stuff)  

misc commands to be run in misc lxterminals:  
`alsamixer` (may need a "-c1")  
aplay: be **REALLY** careful about your ears with that one! (I set the volume in alsamixer down to 10 and it's still rather loud)  
`aplay -f cd -D dmix:CARD=Audio,DEV=2 /dev/urandom`  

Getting sound to work properly: mess around with aplay -L and aplay -l; I found card1 device2 to be working.  
(see also https://forum.armbian.com/topic/4807-tinker-board-sound/, they use device2)  

add device to pulseaudio  
`pactl load-module module-alsa-sink device=hw:1,2 sink_name=test`  
(I got "23" as answer (23rd module loaded - it worked!) )  

`pacmd list-sinks`  

set output:  
`pactl set-default-sink test`  
if aplay /dev/urandom (very loud again!) works you can probably write this setting to a config file somewhere. (I don't need audio so I won't do that.)  

Watching youtube videos in firefox seems to work; moving the window leads to a bit of lag, fullscreen 1080p videos seem to tear and lag quite a bit sometimes.  
Not bad for such a little device without proprietary software (unless I overlooked anything but the boot ROM inside the processor)!  


